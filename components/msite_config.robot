*** Settings ***
Library     Selenium2Library

Documentation   configuration chrome and safari with user agent for access msite m.kumparan.com


*** Variable ***
${URL}              https://m.kumparan.com
${logo_kumparan}    xpath=//span[@class="brand-logo"]

*** Testcases ***
test
    Open Chrome

*** Keywords ***
Open Safari
    Open Browser   ${URL}      Safari

Open Chrome
    ${mobile emulation}=    Create Dictionary    deviceName=iPhone 6/7/8
    ${chrome options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome options}    add_experimental_option    mobileEmulation    ${mobile emulation}
    Create Webdriver    Chrome    chrome_options=${chrome options}
    Goto    ${URL}
    Wait Until Element Is Visible    ${logo_kumparan}
